package CT.FS;
import org.junit.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObjects.LandingPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class SampleSteps {

	WebDriver driver = new ChromeDriver();
	
	
	@Given("^User goes to ctfs$")
	public void user_goes_to_ctfs() throws Throwable {
		driver.manage().window().maximize();
	    System.out.println("1");
	    driver.get(LandingPage.url);
	}

	@When("^User attempts to login with bad credentials$")
	public void user_attempts_to_login_with_bad_credentials() throws Throwable {
		System.out.println("2");
		driver.findElement(By.xpath(LandingPage.userNameXPATH)).sendKeys("Bogus Name");
		driver.findElement(By.xpath(LandingPage.passwordXPATH)).sendKeys("Bogus PW");
		driver.findElement(By.xpath(LandingPage.loginButtonXPATH)).click();
	}

	@Then("^User is prompted that the credentials were wrong$")
	public void user_is_prompted_that_the_credentials_were_wrong() throws Throwable {
		String temp = driver.getCurrentUrl();
		System.out.println("3 " + temp);
		if (!driver.getCurrentUrl().equals(LandingPage.url))
		{
			Assert.fail("you are on the wrong page");
		}
	    throw new PendingException();
	}

	
	
}
