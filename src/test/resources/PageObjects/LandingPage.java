package PageObjects;

public class LandingPage {
	public static String url = "https://customer.ctfs.com/MyOnlineAccount/";
	
	public static String userNameXPATH = "//input[@name='acctnum']";
	public static String passwordXPATH = "//input[@name='pac']";
	public static String loginButtonXPATH = "//input[@type='submit' and @id='Continue']";
}
